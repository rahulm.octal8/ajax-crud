@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Tags</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Tags
                            <a data-toggle="modal" data-target="#exampleModal" class="pull-right"><i
                                    class="fa fa-plus-square fa-lg"></i></a>
                        </div>
                        <div class="card-body">
                            @include('tags.table')
                            <div class="pull-right mr-3">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('tags.templates.template')
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function (){
            $('#tags-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('tags.index') !!}',
                columns: [
                    {
                        data:'title',
                        name:'title',
                    }
                    ,
                    {
                        // data:function (row)
                        // {
                        //     let template = $.templates('#TagTemplate');
                        //     let data = [{'id': row.id}];
                        //     return template.render(data);
                        // },name:'id',

                        data: function (row) {
                            let template = $.templates('#TagTemplate');
                            let data = [{'id': row.id}];
                            return template.render(data);
                        }, name: 'id',
                    }
                ]
            });
        });
    </script>
@endpush
