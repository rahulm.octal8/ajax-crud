<div class="table-responsive-sm">
    <table class="table table-striped" id="projects-table">
        <div class="row">
                <thead class="col col-md-6 col-sm-3">
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class="col col-md-6 col-sm-3">
                </tbody>
            </div>
    </table>
</div>
