@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Projects</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Projects
                            <a class="pull-right" data-toggle="modal" data-target="#exampleModal"><i
                                    class="fa fa-plus-square fa-lg"></i></a>
                        </div>
                        <div class="card-body">
                            @include('projects.table')
                            <div class="pull-right mr-3">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('projects.create')
        @include('projects.templates.template')
        @include('projects.edit')
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let IndexUrl = "{{route('projects.index')}}";
            $('#summernote').summernote();
            $('#projects-table').DataTable({
                ajax: "{{route('projects.index')}}",
                processing: true,
                serverSide: true,
                columnDefs : [
                    {
                        'targets': [0],
                        // 'width':'',
                    },
                    {
                        'targets':[3],
                        'width':'5 %',
                        'orderable': false,
                        'className':'text-center',
                    }
                ],
                columns: [

                    {
                        data: 'title',
                        name: 'title',
                    },
                    {
                        data: 'description',
                        name: 'description',
                    },
                        {{--{--}}
                        {{--    data: function (row){--}}
                        {{--       let data =  row.description;--}}
                        {{--       let Record =  $('#summernote').summernote('code',data);--}}
                        {{--       return {!!  'Record' !!};--}}
                        {{--    },name:'description',--}}
                        {{--},--}}
                    {
                        data: 'status',
                        name: 'status',
                    },
                    {
                        data: function (row) {
                            let template = $.templates('#projectTemplate');
                            let data = [{'id': row.id}];
                            return template.render(data);
                        }, name: 'id',
                    },
                ]
            });
            $(document).on('submit', '#FormCreate', function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{route('projects.store')}}",
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result.success) {
                            $('#exampleModal').modal('hide');
                            $('#projects-table').DataTable().ajax.reload(null, false);
                            $('#FormCreate')[0].reset();
                            $.toast({
                                heading: 'Details',
                                text: 'Record Added SuccessFully',
                                position: 'top-right',
                                stack: false
                            })
                        }
                    },
                    error: function (e) {
                        $.toast({
                            heading: 'Error',
                            text: 'Please Input All Filed' + e,
                            showHideTransition: 'fade',
                            position: 'top-right',
                            icon: 'error'
                        })
                    }
                });
            });

            // edit code
            $(document).on('click', '.editBtn', function (e) {
                e.preventDefault();
                let id = $(this).attr('data-id');
                $('#projectId').val(id);
                $.ajax({
                    url: IndexUrl + '/' + id + '/' + 'edit',
                    type: 'GET',
                    success: function (result) {
                        if (result.success) {
                            $('.title').val(result.data.title);
                            $('.description').summernote('code', result.data.description);
                        }
                    }
                });
            });
            $(document).on('submit', '#FormEdit', function (e) {
                e.preventDefault();
                let id = $('#projectId').val();
                $.ajax({
                    url: IndexUrl + '/' + id,
                    type: 'PUT',
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result.success) {
                            $('#EditModal').modal('hide');
                            $('#projects-table').DataTable().ajax.reload(null, false);
                            $('#FormEdit')[0].reset();
                            $.toast({
                                heading: 'Success',
                                text: 'Update SuccessFully Record',
                                showHideTransition: 'slide',
                                position: 'top-right',
                                icon: 'success'
                            })
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.toast({
                            heading: 'Error',
                            text: 'Please Input All Filed' + e,
                            showHideTransition: 'fade',
                            position: 'top-right',
                            icon: 'error'
                        })
                    }
                });
            });

            // delete code
            $(document).on('click', '.deleteBtn', function (e) {
                let id = $(this).attr('data-id');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this  file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal(" Your  file has been deleted!", {
                                icon: "success",
                            });
                            $.ajax({
                                url: IndexUrl + '/' + id,
                                type: 'DELETE',
                                success: function (result)
                                {
                                    if (result.success)
                                    {
                                        $('#projects-table').DataTable().ajax.reload(null,false);0
                                    }
                                }
                            });
                        } else {
                            swal("Your  file is safe!");
                        }
                    });

            });
        });
    </script>
@endpush
