<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Project
 * @package App\Models
 * @version March 13, 2021, 6:20 pm UTC
 *
 * @property string $title
 * @property string $description
 * @property integer $status
 */
class Project extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'projects';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'description',
//        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'status' => 'exit'
    ];
    const status = [
      1 => 'Pending',
      2 => 'Finished',
    ];
    protected $append=['status_name'];
    public function getStatusNameAttribute()
    {
        return self::status;
    }

}
