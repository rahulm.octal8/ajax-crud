<?php


namespace App\Queries;


use App\Models\Tag;

class TagDataTable
{
    public function get()
    {
       $query =  Tag::all();
        return $query;
    }
}
