<?php


namespace App\Queries;


use App\Models\Project;

class ProjectDataTable
{
    public function get()
    {
        $query = Project::all();
        return $query;
    }
}
